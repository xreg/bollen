﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Tooltip("This singleton instance")]
    public static GameManager instance = null;

    [Tooltip("if accelerometer is found")]
    [SerializeField] private bool accelerometerFound = false;


    [Tooltip("if true print Debug.Log, and Errors")]
    [SerializeField] private bool debug = false;
    [Tooltip("if true print Debug.Log, and Errors in Player script")]
    public bool debugPlayer = false;

    [Tooltip("if true print Debug.Log, and Errors in EnemyAttacker script")]
    public bool debugEnemyAttacker = false;


    [Tooltip("if true print Debug.Log, and Errors in TrackManager script")]
    public bool debugTrackManager;


    private GameSettings gameSettings;
    private TrackManager trackManager;
    private Player player;
    private bool gameOn = false;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {

        gameSettings = GameSettings.instance;
        if (!gameSettings.joystick) GameObject.Find("Fixed Joystick").SetActive(false);

        if (debug)
        {
            Debug.Log("SystemInfo.deviceType.Desktop: " + DeviceType.Desktop);
            
        }

        if (SystemInfo.supportsAccelerometer)
        {
            accelerometerFound = true;
            if (debug) Debug.Log("Accelerometer found");
        } else
        {
            if (debug) Debug.LogError("Accelerometer NOT found");
            if (debug) Debug.LogError("NOTE! if executed through Unity Remote 5 accelerometer is not found");
            accelerometerFound = false;
            //Application.Quit();
            
        }

        player = FindObjectOfType<Player>();
        trackManager = FindObjectOfType<TrackManager>();
        

    }



    // Update is called once per frame
    void Update()
    {
        if (!gameOn)
        {
            trackManager.GenerateStartTrack();
            gameOn = true;

        }

        if (gameOn)
        {
            if (player.transform.position.z > trackManager.currentTrackPart.endZ)
            {
                StartCoroutine(trackManager.EnqueueNewPart());
            }

        }

        
    }
}
