﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackManager : MonoBehaviour
{
    //[Tooltip("Length of track parts list")]
    //[SerializeField] int trackLength = 5;
    

    [SerializeField] Queue<TrackPart> track;

    [SerializeField] GameObject trackPartPreFab;
    [SerializeField] GameObject enemyAttackerPreFab;
    [SerializeField] GameObject enemyPushPreFab;
    [SerializeField] GameObject enemyFlyAndShootPreFab;
    private GameManager gameManager;
    private GameSettings gameSettings;

    private GameObject trackPartsParent;
    private GameObject enemiesParent;
    private bool debug;
    public static int trackID = 0;
    [Tooltip("the current part player is on")]
    public TrackPart currentTrackPart;
    [Tooltip("the zpos trackmanager is on, and to which generate a new par")]
    [SerializeField] float trackZPos = 0;
    [Tooltip("This singleton instance")]
    public static TrackManager instance = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            track = new Queue<TrackPart>();
        }
        else if (instance != this)
            Destroy(gameObject);
        
    }
    void Start()
    {
        gameManager = GameManager.instance;
        gameSettings = GameSettings.instance;
        trackPartsParent = GameObject.Find("TrackPartsParent");
        enemiesParent = GameObject.Find("EnemiesParent");
        debug = gameManager.debugTrackManager;
            
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateStartTrack()
    {
        

        for (int i = 0; i < gameSettings.tpQueueSize; i++) {

            
            TrackPartParameters tpm = new TrackPartParameters();
            tpm.numVertices = 100;
            tpm.minXdifference = 10 - i ;
            tpm.leftWave = 10 + i * 50;
            tpm.rightWave = 15 - i *2;
            tpm.zPos = trackZPos;
            tpm.xPos = 0;
            tpm.yPos = -2 + i ;
            tpm.id = trackID;
            tpm.numEnemies = 5 + trackID;

            tpm.type = (TrackTypes.type)Random.Range(1, 3);
            TrackPart tp = GenerateTrackPart(tpm);
            trackID++;
            trackZPos += tp.zLength + 10;
            track.Enqueue(tp);
            if (i == 0) currentTrackPart = tp;
        }
        

    }
/* Insert a trackpart to the list and removes the last oven
 * 
 * 
 */


    public IEnumerator EnqueueNewPart()
    {
        TrackPart tpDe = track.Dequeue();
        foreach (GameObject g in tpDe.enemies)
        {
            Destroy(g);
        }
        Destroy(tpDe.gameObject);
        currentTrackPart = track.Peek();
        if (debug) Debug.Log(" queue count after Dequeue: " + track.Count);
        TrackPartParameters tpm = new TrackPartParameters();
        tpm.numVertices =(int) (100 + 80 * Mathf.Sin(trackID));
        tpm.minXdifference = 10 + 5 * Mathf.Sin(trackID);
        tpm.leftWave = 15 * Mathf.Sin(trackID);
        tpm.rightWave = 15 * Mathf.Cos(trackID);
        tpm.xPos = 15f * Mathf.Sin(trackID);
        tpm.zPos = trackZPos;
        tpm.yPos = 5 + 7 * Mathf.Sin(trackID);
        tpm.numEnemies = 1 + trackID;
        tpm.id = trackID;
        tpm.type = (TrackTypes.type)Random.Range(1, 3);
        TrackPart tp = GenerateTrackPart(tpm);
        trackID++;

        trackZPos += tp.zLength + 10;

        track.Enqueue(tp);
        if (debug) Debug.Log(" queue count after enqueue: " + track.Count);
        yield return null;
    }


     TrackPart GenerateTrackPart(TrackPartParameters param)
    {
        GameObject go = Instantiate(trackPartPreFab);
        go.transform.SetParent(trackPartsParent.transform, false);
        go.transform.position = new Vector3(param.xPos, param.yPos, param.zPos);
        TrackPart trackPart = go.GetComponent<TrackPart>();
        List<Vector3> verticesList = new List<Vector3>();
        float zp = 0;
        float waveAngle = 0;
        int everyZAddEnemy = param.numVertices / param.numEnemies;
        int everyZAddCounter = 0;
        // 2 vertices is added in each iterations
        
        for (int z = 0; z<param.numVertices / 2; z++)
        {

            TrackPoint trackPoint = new TrackPoint();
            waveAngle += z * .01f;
            float mid = 0;
            float xLeft = mid -(2f + (Mathf.Abs(Mathf.Sin(waveAngle)) * param.leftWave)); 
            float xRight = mid + 2f + (Mathf.Abs(Mathf.Cos(waveAngle)) * param.rightWave);


            //if (xLeft > xRight) xLeft -= xRight;
            //if (xRight < xLeft) xRight += xLeft;

            if (Mathf.Abs(xLeft-xRight) < param.minXdifference)
            {
                xLeft = -param.minXdifference;
                xRight = param.minXdifference;
            }
            
            if (debug) Debug.Log("xLeft: " + xLeft + "  xRight" + xRight);

            zp = z;
            trackPoint.left = new Vector3(xLeft, 0, zp);
            trackPoint.right = new Vector3(xRight, 0, zp);

            verticesList.Add(trackPoint.left);
            verticesList.Add(trackPoint.right);

            // calc checkPointPos
            if (z == 0)
            {
                trackPart.checkPointPos = new Vector3(param.xPos+ (xLeft + xRight) / 2, param.yPos + 1, param.zPos + 2);
            }

            if (everyZAddCounter> everyZAddEnemy)
            {
                Vector3 spawnPoint = new Vector3(param.xPos + Random.Range(0f, xRight), param.yPos + 1, param.zPos + zp);
                GameObject enemy;
                int r = Random.Range(0, 3);
                if (r == 0)
                {
                    enemy = Instantiate(enemyAttackerPreFab);
                } else if(r == 1)
                {
                    enemy = Instantiate(enemyPushPreFab);
                } else
                {
                    spawnPoint = new Vector3(param.xPos + Random.Range(0f, xRight), param.yPos + 10, param.zPos + zp);
                    enemy = Instantiate(enemyFlyAndShootPreFab);
                }
                enemy.transform.SetParent(enemiesParent.transform, false);
                enemy.transform.position = spawnPoint;
                trackPart.enemies.Add(enemy);
                everyZAddCounter = 0;
            }
            everyZAddCounter++;

        }
        trackPart.zLength = zp;
        trackPart.endZ = param.zPos + zp;

        if (debug) Debug.Log("Length of verticesList: " + verticesList.Count);
        trackPart.mesh.vertices = verticesList.ToArray();
        GenerateTriangles(ref trackPart.mesh, verticesList.Count * 6, param.numVertices / 2);

        trackPart.mesh.RecalculateNormals();
        trackPart.mesh.RecalculateBounds();
        trackPart.mesh.RecalculateTangents();

        
        trackPart.gameObject.AddComponent<MeshCollider>();
        trackPart.id = param.id;
        trackPart.type = param.type;
        trackPart.gameObject.name = "TrackPart" + param.id.ToString();
        trackPart.GenerateNavMesh();
        return trackPart;
    }
        


    

    private void GenerateTriangles(ref Mesh mesh, int numTriangles, int trackSize)
    {
        int[] triangles = new int[numTriangles];
        int ti = 0;
        int xSize = 2;
        int vi = 0;
        for (int z = 0; z < trackSize - 1; z++)
        {
            {
                triangles[ti + 0] = vi + 0;
                triangles[ti + 1] = vi + xSize;
                triangles[ti + 2] = vi + xSize + 1;
                triangles[ti + 3] = vi + 0;
                triangles[ti + 4] = vi + xSize + 1;
                triangles[ti + 5] = vi + 1;
               /*
                // DOES NOT WORK????????? 
                ti = ti + 6;
                
                triangles[ti + 0] = vi + 0;
                triangles[ti + 1] = vi + 1;
                triangles[ti + 2] = vi + xSize + 1;
                triangles[ti + 3] = vi + 0;
                triangles[ti + 4] = vi + xSize + 1;
                triangles[ti + 5] = vi + xSize;
                */
                vi = vi + 2;

                ti = ti + 6;


            }

        }
        mesh.triangles = triangles;
    }
}
