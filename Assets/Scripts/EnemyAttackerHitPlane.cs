﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackerHitPlane : EnemySuper
{
    
    void Start()
    {
        base.Start(); 

    }

    // Update is called once per frame
    void Update()
    {
    
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (debug) Debug.Log("HitPLane onCollisionEnter");
        if (collision.collider.name == "Player")
        {
            if (debug) Debug.Log("collider.name == Player");
            if (debug) Debug.Log("Destroy Enemy player hit the HitPlane");
            Destroy(transform.parent.gameObject);
        }
    }
}
