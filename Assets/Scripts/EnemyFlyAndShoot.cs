﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlyAndShoot : EnemySuper
{
    private bool playerInRange = false;
    [SerializeField] GameObject bulletPreFab;
    private float timer = 0;
    private float xa;
    private float ya;
    private float za;
    void Start()
    {
        xa = Random.Range(1, 12);
        ya = Random.Range(1, 12);
        za = Random.Range(1, 12);
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        xa += Time.deltaTime;
        ya += Time.deltaTime;
        za += Time.deltaTime;
        transform.position = new Vector3(transform.position.x + 0.035f * Mathf.Sin(xa), transform.position.y+0.025f * Mathf.Cos(ya), transform.position.z+ 0.03f * Mathf.Sin(za));
        if (Vector3.Distance(transform.position, player.transform.position) < gameSettings.enemyFlyAndShootPlayerDistance)
        {
            playerInRange = true;
        } else
        {
            playerInRange = false;
        }

        if (playerInRange)
        {
            timer += Time.deltaTime;
            if (timer > gameSettings.enemyFlyAndShootFireInterval)
            {
                GameObject bullet = Instantiate(bulletPreFab);
                
                Vector3 dir = (transform.position - player.transform.position).normalized;
                bullet.GetComponent<EnemyBullet>().dir = dir;
                bullet.transform.position = transform.position + dir;

                timer = 0;
            }

        }
       
    }


}
