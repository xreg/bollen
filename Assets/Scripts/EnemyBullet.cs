﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public Vector3 dir;
    private GameSettings gameSettings;
    void Start()
    {
        gameSettings = GameSettings.instance;
    }

    // Update is called once per frame
    void Update()
    {

        transform.position -= dir * Time.deltaTime * gameSettings.enemyFlyAndShootBulletSpeed;

    }
}
