﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Joystick joystick;
    // Start is called before the first frame update
    private GameManager gameManager;
    private GameSettings gameSettings;
    private TrackManager trackManager;
    private bool debug;
    private Rigidbody rb;
    private Vector3 initPos;
    private float touchHorizontal = 0;
    private bool airborne = false;
    void Start()
    {
        gameManager = GameManager.instance;
        gameSettings = GameSettings.instance;
        trackManager = TrackManager.instance;
        debug = gameManager.debugPlayer;
        rb = GetComponent<Rigidbody>();
        initPos = transform.position;
        rb.velocity = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {

        if (debug) Debug.Log("Input accelerometer" + Input.acceleration);
        //rb.AddForce(moveHorizontal)
        if (gameSettings.movePlayerTransform) transform.Translate(Input.acceleration.x, 0, Input.acceleration.z);

        /*
        if (gameSettings.touchMove)
        {
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    touchHorizontal = Input.GetTouch(0).position.y;
                    if (debug) Debug.Log(" Began pahse " + touchHorizontal);
                }
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    touchHorizontal += Input.GetTouch(0).position.y;
                    if (debug) Debug.Log(" Ended pahse " + touchHorizontal);
                }

            }

        }
        */
        if (gameSettings.movePlayerRigidBody)
        {
            float moveHorizontal;
            float moveVertical;
            if (gameSettings.joystick)
            {
                moveHorizontal = joystick.Vertical;
                moveVertical = joystick.Horizontal;
                Debug.Log(" Horizontal: " + moveHorizontal + " vertical " + moveVertical);
            } else
            {
                moveHorizontal = Input.acceleration.y;
                moveVertical = Input.acceleration.x;
            }

            if (debug)
            {
                Debug.Log(" Horizontal: " + moveHorizontal + " vertical " + moveVertical);
            }
            float yForce = 0;
            // FIND A BETTER SOLUTION FOR THIS???!!!!!
            if ((Mathf.Abs(rb.velocity.y) < .5f))
            {
                airborne = false;
            }
            else
            {
                airborne = true;
            }
            /*if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    if (gameSettings.allowAirJump)
                    {
                        yForce = gameSettings.jumpForce;
                    }
                    else if (!airborne)
                    {
                        yForce = gameSettings.jumpForce;
                    }
                }
            }*/
            // if Z velocity > max set moveHorizontal to 0, 
            if (Mathf.Abs(rb.velocity.z) > gameSettings.playerMaxZVelocity) moveHorizontal = 0;

            // if X velocity > max set moveVertical to 0, 
            if (Mathf.Abs(rb.velocity.x) > gameSettings.playerMaxXVelocity) moveVertical = 0;

            if (airborne)
            {
                rb.AddForce(new Vector3(moveVertical * gameSettings.airborneForceMultiplier, yForce, moveHorizontal * gameSettings.airborneForceMultiplier), ForceMode.Impulse);
            } else
            {
                rb.AddForce(new Vector3(moveVertical * gameSettings.forceXMultiplier, yForce, moveHorizontal * gameSettings.forceZMultiplier), ForceMode.Impulse);
            }

            if (airborne)
            {
                rb.AddForce(new Vector3(0, -gameSettings.airborneGravity, 0), ForceMode.Impulse);

            }

            if (!gameSettings.allowReverse) if (rb.velocity.z < 0) rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 0);
            

        }



        if (gameSettings.keyboardInput)
        {
            // NOTE NOTE NOTE Horizontal and vertical not on correct places, maybe how phone was last rotated.
            float moveVertical = Input.GetAxis("Horizontal");
            float moveHorizontal = Input.GetAxis("Vertical");
            
            float yForce = 0;
            if (Input.GetKeyDown(KeyCode.Space)) yForce = gameSettings.jumpForce;
            rb.AddForce(new Vector3(moveVertical * gameSettings.forceXMultiplier, yForce, moveHorizontal * gameSettings.forceZMultiplier), ForceMode.Impulse);
        }
        // death temp fix!
        if (transform.position.y < -3)
        {
            //initPosition();
            transform.position = trackManager.currentTrackPart.checkPointPos;
            rb.velocity = Vector3.zero;

        }

    }
    public void Jump()
    {
        if (airborne && !gameSettings.allowAirJump)
        {
            return;
        }
        rb.AddForce(new Vector3(0, gameSettings.jumpForce, 0), ForceMode.Impulse);
        Debug.Log(" JUMP ");
    }
    private void initPosition()
    {

        transform.position = initPos;
        rb.velocity = Vector3.zero;
    }


}
