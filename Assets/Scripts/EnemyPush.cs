﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyPush : EnemySuper
{
    NavMeshAgent agent;
    float agentUpdateTimer = 0;
    void Start()
    {
        base.Start();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(player.transform.position, transform.position) < gameSettings.enemyPushPlayerDistance)
        {
            agent.SetDestination(player.transform.position);
            agent.speed = gameSettings.enemyPushSpeed;
        }
    }
}
