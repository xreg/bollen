﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
/*
 * 
 */
public class TrackPoint : MonoBehaviour
{
    public Vector3 left;
    public Vector3 right;
    
}
