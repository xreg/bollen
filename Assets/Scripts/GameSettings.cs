﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    [Header("Game Global Settings")]
    [Tooltip("How Many trackparts are simultanesly in the queue")]
    public int tpQueueSize = 5;
    [Header("Player Settings")]
    public bool movePlayerTransform;
    public bool movePlayerRigidBody;
    // public[ bool touchMove;
    public bool keyboardInput;
    public bool joystick;

    [Tooltip("multiplies the X acceleration movement with this. < 1 slows >1 increases, NOTE only affect the rigidbody movement")]
    [Range(0.1f, 2)]
    public float forceXMultiplier = 1;
    [Tooltip("multiplies the Z acceleration movement with this. < 1 slows >1 increases, NOTE only affect the rigidbody movement")]
    [Range(0.1f, 2)]
    public float forceZMultiplier = 1;

    [Tooltip("Max Z velocity")]
    [Range(1f, 50)]
    public float playerMaxZVelocity = 25;
    [Tooltip("Max X velocity")]
    [Range(1f, 50)]
    public float playerMaxXVelocity = 25;



    
    [Tooltip("Is set to True player can jump if y velocity >0 (in the air)")]
    public bool allowAirJump = false;
    [Tooltip("Is set to True player reverse, ie. z velocity can be less than 0")]
    public bool allowReverse = false;
    [Range(0.1f, 1)]
    [Tooltip("The multiplier of force when airborne.  ")]
    public float airborneForceMultiplier = 1;
    [Range(0.1f, 2)]
    [Tooltip("The amount of force that is added to the drag when player airborne. \" extra gravity \"  ")]
    public float airborneGravity = 1;
    [Tooltip("This singleton instance")]
    public static GameSettings instance = null;


   
    [Tooltip("The force added in jump to the Y axis ")]
    [Range(0.1f, 50)]
    public float jumpForce = 1;

    [Header("EnemyAttacker settings")]
    [Tooltip("Distance to player when starts hunt")]
    [Range(1, 50)]
    public float enemyAttackerHuntDistance = 10;
    [Tooltip("Speed the EnemyAttacker hunts the player")]
    [Range(1, 10)]
    public float enemyAttackerHuntSpeed = 1;
    [Tooltip("How often the EnemyAttacker update agent target when hunt")]
    [Range(1, 10)]
    public float enemyAttackerUpdateAgentTime = 1;
    [Header("Enemy Push settings")]
    [Range(1.0f, 10.0f)]
    [Tooltip("Distance when player becomes visible to the enemy")]
    public float enemyPushPlayerDistance;
    [Tooltip("Speed the EnemyAttacker hunts the player")]
    [Range(1, 10)]
    public float enemyPushSpeed = 1;
    [Header("Enemy fly and shoot settings")]
    [Range(1.0f, 50.0f)]
    [Tooltip("Distance when player becomes visible to the enemy")]
    public float enemyFlyAndShootPlayerDistance;
    [Tooltip("shooting intervall when player in distance")]
    [Range(1f, 5f)]
    public float enemyFlyAndShootFireInterval;
    [Tooltip("bullet speed multiplier")]
    [Range(1f, 50f)]
    public float enemyFlyAndShootBulletSpeed;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    
}
