﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemySuper : MonoBehaviour
{
    protected GameManager gameManager;
    protected GameSettings gameSettings;
    protected Player player;
    protected bool debug;
    public void Start()
    {
        gameManager = GameManager.instance;
        gameSettings = GameSettings.instance;
        player = FindObjectOfType<Player>();
        debug = gameManager.debugEnemyAttacker;


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
