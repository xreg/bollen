﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TrackPart : MonoBehaviour
{
    

    private Vector3[] vertices { get => vertices; set => vertices = value; }
    

    private Vector3[] normals;
    private int[] triangles;
    private Vector2[] uvs;    
    public Mesh mesh;

    // after player has passed this next part ...
    public float endZ;
    public float zLength;
    public int id;
    public TrackTypes.type type = TrackTypes.type.Wave;
    public Vector3 checkPointPos;
    public List<GameObject> enemies;
    private MeshCollider meshCollider = null;
    private NavMeshSurface navMeshSurface;
    
    private void Awake()
    {
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "TrackPart";
        enemies = new List<GameObject>();
        
    }

    void Start()
    {
        
        if (type == TrackTypes.type.Wave)
        {
            mesh.MarkDynamic();
        }

    }

    public void GenerateNavMesh()
    {        
        gameObject.AddComponent<NavMeshSurface>();
        navMeshSurface = GetComponent<NavMeshSurface>();
        navMeshSurface.BuildNavMesh();
    }
    public void SetPosition(Vector3 pos)
    {
        transform.position = pos;
    }
    // Update is called once per frame
    void Update()
    {

        if (type == TrackTypes.type.Wave)
        {
            if (meshCollider == null)
            {
                meshCollider = GetComponent<MeshCollider>();
                // Debug.Log("MeshCollider " + meshCollider);
            }
            else
            {

                Vector3[] vertices = mesh.vertices;
                Vector3[] newVertices = new Vector3[vertices.Length];
                for (int i = 0; i < mesh.vertexCount; i++)
                {
                    newVertices[i] = new Vector3(vertices[i].x, vertices[i].y + 0.005f * Mathf.Sin(Time.time + vertices[i].z), vertices[i].z);

                }
                mesh.vertices = newVertices;
                meshCollider.sharedMesh = mesh;
                //mesh.RecalculateNormals();
                mesh.RecalculateBounds();
                //navMeshSurface.BuildNavMesh();
            }
        }

    }
    
}
