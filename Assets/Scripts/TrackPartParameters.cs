﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class TrackPartParameters : MonoBehaviour
{
    
    public int numVertices;
    public TrackTypes.type type;
    public float minXdifference;

    public float leftWave;
    public float rightWave;

    public float xPos;
    public float yPos;
    public float zPos;
    public int id;
    public int numEnemies;
    


}
