﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAttacker : EnemySuper
{   
   
    [SerializeField] int state = 0;
    [SerializeField] bool isHunt = false;
    [SerializeField] SphereCollider sphereCollider;
    [SerializeField] BoxCollider boxCollider;
    NavMeshAgent agent;
    float agentUpdateTimer = 0;
    void Start()
    {

        base.Start();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isHunt)
        {
            if (Vector3.Distance(player.transform.position, transform.position) < gameSettings.enemyAttackerHuntDistance)
            {
                agent.SetDestination(player.transform.position);
                agent.speed = gameSettings.enemyAttackerHuntSpeed;
                isHunt = true;
            }
        } else
        {
            agentUpdateTimer += Time.deltaTime;
            if (agentUpdateTimer > gameSettings.enemyAttackerUpdateAgentTime)
            {
                agent.SetDestination(player.transform.position);
                agent.speed = gameSettings.enemyAttackerHuntSpeed;
                agentUpdateTimer = 0;
            }
        }
       /* if (!isHunt) {
            if (Vector3.Distance(player.transform.position, transform.position) < gameSettings.enemyAttackerHuntDistance)
            {
                isHunt = true;
            }
        } else
        {

            if (state == 0) transform.position = Vector3.MoveTowards(transform.position, player.transform.position, gameSettings.enemyAttackerHuntSpeed * Time.deltaTime);
            if (Vector3.Distance(player.transform.position, transform.position) >= gameSettings.enemyAttackerHuntDistance)
            {
                isHunt = false;
            }
        }

        RaycastHit hit;
        Vector3 rayDir = Vector3.Normalize(player.transform.position);//new Vector3(0, .1f, 1);
        if (Physics.Raycast(transform.position, transform.TransformDirection(rayDir), out hit, Mathf.Infinity)){
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.red);
            state = 0;
        } else
        {
            //if (hit.transform.gameObject.tag == "TrackPart")
            //{
                state = 1;
            //}

            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.yellow);

        }*/

    }


}
